# Minecraft Gateway

Minecraft Gateway is a Layer 7 reverse proxy for the Minecraft protocol

## Goal
The goal of this project was to:
- Handle Minecraft players connections and route them internaly to the corresponding server
- Keep track of connected players and send them to the Minekloud API
- Under certain conditions, start a stopped server when a player try to join

## How it works

For the Minekloud project we provided each server with a subdomain ex: `foo.play.minekloud.com` and `bar.play.minekloud.com`. Every subdomain resolve to the same address (i.e.: the Minecraft Gateway).

There are no way to known at the TCP level (Layer 4) wich domain name was used (the dns resolution is done client side). Thanksfully the first minecraft packet send by the client called [handshake](https://wiki.vg/Protocol#Handshaking) contain a `Server Address` field corresponding to the hostname the player used is the game GUI. This way we known if the player used `foo` or `bar` subdomain.

This proxy is different from [BungeeCord](https://github.com/SpigotMC/BungeeCord/) or [Waterfall](https://github.com/PaperMC/Waterfall) because we only read packets when the connection is initializing. Once the connection is established we forward every data between the player and server. It's transparent and we don't modify the connection encryption between client and server. 

One of the downside is that for the server every player share the same ip (the Minecraft Gateway IP) so you can't ban player by IP (well I guess nothing technicaly prevent you but it will ban everyone). One way to improve this with `Spigot` and [Paper](https://github.com/PaperMC/Paper) server will be to use the build in `ip_forward` feature built for `BungeeCord`.

## Installation
Clone the repository and install the dependencies with npm (or yarn)

```bash
npm ci
```

Start the program with npm (or yarn)

```bash
npm run start
```

(Or you wan use the docker image `registry.gitlab.com/cpelyon/5irc-minekloud/minecraft-gateway:latest`)

## Configuration
You can use:
- `config.json` file
- `DEFAULT_SERVER_HOST` and `DEFAULT_SERVER_PORT` env var (apply to `localhost` only)
- `JSON_CONFIG` env var
- Minekloud Servers API `DEFAULT_SERVERS_SERVICE_ADDR`


## Similar projects

itzg/mc-router: https://github.com/itzg/mc-router
