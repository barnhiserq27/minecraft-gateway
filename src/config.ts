import { readFileSync } from 'fs'
import { Server } from './server/models/Server'

export interface ProxyRoute {
  externalHostname: string
  internalHostname: string
  internalPort: number
  server?: Server
}

class Config {

  serveurList = new Map<string, ProxyRoute>()

  constructor() {
    this.loadFromFile()
    //this.loadFromEnv()
  }

  loadFromFile(): void {
    const rawconfig = readFileSync('config.json', 'utf8')
    this.addRoutes(JSON.parse(rawconfig))
  }

  loadFromEnv(): void {
    // Config from ENV DEFAULT_SERVER_HOST
    if (process.env.DEFAULT_SERVER_HOST) {
      this.addRoutes([
        {
          externalHostname: 'localhost',
          internalHostname: process.env.DEFAULT_SERVER_HOST,
          internalPort: Number(process.env.DEFAULT_SERVER_PORT) || 25565,
        },
      ])
    }

    // ENV as json
    const jsonEnvConfig = process.env.JSON_CONFIG
    if (jsonEnvConfig) {
      this.addRoutes(JSON.parse(jsonEnvConfig))
    }
  }

  addRoutes(routes: ProxyRoute[]): void {
    routes.forEach((e) =>
      this.serveurList.set(e.externalHostname, {
        ...e,
        internalPort: e.internalPort || 25565,
      })
    )
  }
}

export default new Config()