import ServerApi from '../server/ServerApi'

class PlayerManager {
  private serversPlayersMap: Map<number, Set<string>> = new Map()

  handlePlayerConnection(serverId: number, username: string) {
    console.log(
      `Player with username <${username}> join server with id  ${serverId}`
    )
    const usernames = this.serversPlayersMap.get(serverId) || new Set<string>()
    usernames.add(username)

    ServerApi.postPlayers(serverId, [username])
  }

  handlePlayerDisconnection(serverId: number, username: string) {
    console.log(
      `Player with username <${username}> disconnect from server id ${serverId}`
    )
    const usernames = this.serversPlayersMap.get(serverId)

    usernames?.delete(username)

    ServerApi.deletePlayer(serverId, username)

    if (usernames?.size == 0) {
      this.serversPlayersMap.delete(serverId)
    }
  }

  sendCurrentPlayersConnected() {
    this.serversPlayersMap.forEach((usernames: Set<string>, serverId: number) => {
      ServerApi.postPlayers(serverId, Array.from(usernames))
    })
  }
}

export default new PlayerManager()
